# ~*~ coding: utf-8 ~*~
import redis
import json

#redis 配置信息从项目根目录conf/db.conf中获取
redisPool = redis.ConnectionPool(host=redis_host,port=redis_port,db=redis_db,password=redis_password)
client = redis.Redis(connection_pool=redisPool)
flag = 'icbc'

def send_redis(ip,alarm,item,status):
	# 发送状态到故障自愈系统处理
	ms = {
		'IP': ip,
		'告警项': alarm,
		'自愈项': item,
		'当前状态': status
	}
	vfs = json.dumps(ms)
	client.lpush(flag, vfs)
send_redis(ip,alarm,item,status)

#告警项|对应项目中: 监控中心-监控配置-任务名称
#自愈项|对应项目中: 故障自愈-自愈字典配置-自愈项


'''
根据这个demo把zabbix nagios 日志接入进来
其中 告警项目对应
zabbix告警模板

告警主机:{HOST.NAME}
告警IP:{HOST.IP}
告警时间:{EVENT.DATE}{EVENT.TIME}
告警等级:{TRIGGER.SEVERITY}
告警信息: {TRIGGER.NAME}
告警项目:{TRIGGER.KEY1}
问题详情:{ITEM.NAME}:{ITEM.VALUE}
当前状态:{TRIGGER.STATUS}:{ITEM.VALUE1}
事件ID:{EVENT.ID}


'''
