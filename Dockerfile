FROM hub.c.163.com/netease_comb/centos:7
ENV PATH $PATH:/usr/local/python3/bin/

COPY install/Python-3.6.15.tgz /usr/local
COPY install/pip_list.txt /usr/local
COPY install/locale.conf /etc/locale.conf
#COPY install/i18n /etc/sysconfig/i18n
ENV PYTHONIOENCODING utf-8
RUN set -ex \
	&& mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup \
	&& curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo \
	&& yum makecache \
	&& sed -i -e '/mirrors.cloud.aliyuncs.com/d' -e '/mirrors.aliyuncs.com/d' /etc/yum.repos.d/CentOS-Base.repo \
	&& yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gcc gcc-c++ make wget sshpass kde-l10n-Chinese \
	&& yum clean all \
	&& cd /usr/local && tar -zxf Python-3.6.15.tgz \
	&& cd Python-3.6.15 \
	&& ./configure \
	&& make && make install \
	&& localedef -i zh_CN -f UTF-8 zh_CN.UTF-8 

RUN set -ex \
	&& cd /usr/local && python3 -m venv env \
	&& source env/bin/activate \
	&& export LANG="zh_CN.utf8" \
	&& pip install --upgrade pip -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com \
     	&& pip install -r pip_list.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com 

#CMD ["tail","-f","/etc/yum.repos.d/CentOS-Base.repo"]
CMD ["sh","/ncc/service/fsr/cmdb/docker_start.sh","start"]
