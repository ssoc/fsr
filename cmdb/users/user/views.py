# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django.shortcuts import render
from django.views.generic import ListView, TemplateView, View, DetailView
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import Group, User, Permission
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.shortcuts import redirect
from django.forms.models import model_to_dict
from django.shortcuts import render_to_response,HttpResponse

from users.forms import CreateUserForm, CreateProfileForm
from users.models import Profile, RegisterEmail,GroupProfile

from alert.models import AlertConfig
from utils.sendmail import send_mail
from utils.password_url_hash import random_str
from confs.Log import logger
import datetime
from confs.Configs import *
from django.contrib.auth.hashers import check_password

# 用户列表

class UserAPIJsonView(LoginRequiredMixin, ListView):
    def get(self,request):
        jsondata = {
			"code": 0,
			"msg": "",
			"count": 10,
			"data": [
			]
		}
        users = Profile.objects.all()
        for s in users:
            m = {"id": s.id,"name":s.name,"username":s.user.username,"email":s.user.email,"phone":s.phone,
				 "weixin":s.weixin,"is_active":s.user.is_active}
            jsondata["data"].append(m)

        return JsonResponse(jsondata)


class UserListView(LoginRequiredMixin, ListView):
	template_name = 'user/user_list.html'
	#model = Profile
	#paginate_by = 5
	#ordering = 'id'

	def get(self, request):
		users_list = Profile.objects.all()
		logger.info(users_list)
		return render_to_response('user/user_list.html', locals())

	def get_pagerange(self, page_obj):
		current_index = page_obj.number
		max_pages = page_obj.paginator.num_pages + 1
		#print(max_pages)

		start = current_index - 1
		end = current_index + 2

		if end > max_pages:
			start = start - 1
			end = max_pages

		if start < 1:
			if end < max_pages:
				end = end + 1
			start = 1

		return range(start, end)


# 创建用户
class UserCreateView(LoginRequiredMixin, TemplateView):
	template_name = 'user/user_create.html'

	def get(self, request):

		groups = [{"value": m.group.id, "name": m.group.name} for m in GroupProfile.objects.all()]
		jsondata = {"groups": groups}

		return JsonResponse(jsondata)

	def post(self, request):

		ret = {}
		ret['status'] = 0
		ret['msg'] = '创建成功'
		user_form = CreateUserForm(request.POST)
		profile_form = CreateProfileForm(request.POST)
		mail = AlertConfig.objects.first()
		logger.info('配置的邮件地址是%s'%mail.email)
		if not mail.email:
			ret['status'] = 1
			ret['msg'] = '请先在系统管理-报警服务设置中配置邮件服务器信息'
			return JsonResponse(ret)


		if user_form.is_valid() and profile_form.is_valid():

			user = User(**user_form.cleaned_data)
			user.save()
			logger.info('User表OK,,,,')

			#experm = self.request.POST.get('experm')
			user_profile = Profile()
			user_profile.user = user
			user_profile.name = profile_form.cleaned_data['name']
			user_profile.lnvalid_date = profile_form.cleaned_data['lnvalid_date']
			user_profile.phone = profile_form.cleaned_data['phone']
			user_profile.weixin = profile_form.cleaned_data['weixin']
			#user_profile.experm = profile_form.cleaned_data['experm']
			user_profile.info = request.POST.getlist('info', None)[0]
			
			user_profile.save()

			logger.info('userprofile表OK,,,,')
			logger.info(request.POST.get('members', None))
			logger.info(request.POST.getlist('members', None))
			if request.POST.get('members', None):
				user.groups.set(request.POST.getlist('members'))

			register_email = RegisterEmail()
			register_email.user = user
			register_email.type_code = 0
			register_email.code = random_str()
			register_email.save()

			contnet = """
			<p>你好 %s: </p>
			
			<p>恭喜您，您的账号已经创建成功 </p>

			<p>用户名: %s </p>

			<p><a href='%s'>请点击这里设置密码</a> </p>
			""" %(user.username, user_profile.name, HOST_URL + reverse('user_create_password') + '?code=' + str(register_email.code))


			mail_user = mail.email
			mail_password = mail.password
			mail_host = mail.host
			mail_port = mail.port
			mail_title = '[fsr故障自愈系统]'

			try:
				send_mail(mail_user,mail_password,user.email,mail_title,contnet,mail_host,mail_port)
				print('邮件发送OK...')
			except Exception as e:
				print(e)
				print('邮件发送失败...')
		else:
			ret['status'] = 1
			ret['msg'] = '创建失败'

		return JsonResponse(ret)



# 设置密码
class UserConfigPasswordView(LoginRequiredMixin, TemplateView):
	template_name = 'user/user_config_passwd.html'

	def get_context_data(self, **kwargs):
		context = super(UserConfigPasswordView, self).get_context_data(**kwargs)

		code = self.request.GET.get('code', None)

		try:
			register_email_obj = RegisterEmail.objects.get(code=code)
			context['user_obj'] = register_email_obj.user
			context['code'] = code
		except User.DoesNotExist:
			pass

		return context

	def post(self, request):
		ret = {"status": 0}

		code = request.POST.get('code')
		password_1 = request.POST.get('password_1')
		password_2 = request.POST.get('password_2')
		print(request.POST)

		if password_1 == password_2:
			try:
				register_email_obj = RegisterEmail.objects.get(code=code)
				if register_email_obj.active_status == 0:
					register_email_obj.user.set_password(password_1)
					register_email_obj.user.save()
					register_email_obj.active_status = 1
					register_email_obj.save()
					ret['msg'] = '设置密码成功,点击OK后自动跳转登录页'
				else:
					ret['status'] = 1
					ret['msg'] = '激活码异常，请联系管理员'
			except User.DoesNotExist:
				ret['status'] = 1
				ret['msg'] = '没有此用户'
			except Exception:
				ret['status'] = 1
				ret['msg'] = '异常错误，请练习管理员'
		else:
			ret['status'] = 1
			ret['msg'] = '两次密码输入不一样，请重新输入'
		return JsonResponse(ret)


# 删除用户
class UserDeleteView(LoginRequiredMixin, View):

	def post(self, request):
		ret = {'status': 0}
		uid = request.POST.get('user_id', None)
		pr = Profile.objects.get(id=uid)
		try:
			User.objects.get(id=pr.user.id).delete()
			ret['msg'] = '删除用户成功'
		except User.DoesNotExist:
			ret['status'] = 1
			ret['msg'] = '此用户不存在，删除失败'
		except Exception as e:
			ret['status'] = 1
			ret['msg'] = e

		return JsonResponse(ret)


# 禁用用户
class UserStopView(LoginRequiredMixin, View):

	def post(self, request):
		ret = {'status': 0}
		uid = request.POST.get('user_id', None)
		pr = Profile.objects.get(id=uid)

		try:
			user = User.objects.get(id=pr.user.id)
			user.is_active=False
			user.save()

			ret['msg'] = '禁用%s用户成功' %(user.username)
		except User.DoesNotExist:
			ret['status'] = 1
			ret['msg'] = '此用户不存在，禁用失败'
		except Exception:
			ret['status'] = 1
			ret['msg'] = '其他错误，请联系系统管理员'

		return JsonResponse(ret)


# 启用用户
class UserStartView(LoginRequiredMixin, View):

	def post(self, request):
		ret = {'status': 0}
		uid = request.POST.get('user_id', None)
		pr = Profile.objects.get(id=uid)

		try:
			user = User.objects.get(id=pr.user.id)
			user.is_active=True
			user.save()

			ret['msg'] = '启用%s用户成功' %(user.username)
		except User.DoesNotExist:
			ret['status'] = 1
			ret['msg'] = '此用户不存在，启用失败'
		except Exception:
			ret['status'] = 1
			ret['msg'] = '其他错误，请联系系统管理员'

		return JsonResponse(ret)


# 修改用户
class UserModifyView(LoginRequiredMixin, View):

	def get(self, request):
		uid = request.GET.get('id', None)
		s = Profile.objects.get(id=uid)
		groups = Group.objects.all()
		group_list = []
		groupid_list = [x.id for x in s.user.groups.all()]
		for g in groups:
			if g.id in groupid_list:
				group_list.append({"name": g.name, "value": g.id, "selected": True})
			else:
				group_list.append({"name": g.name, "value": g.id})
		print (group_list)
		if s.user.is_active == 1:
			role_list = [{"name": "超级管理员", "value": 1, "selected": True},{"name": "普通用户", "value": 0}]
		else:
			role_list = [{"name": "超级管理员", "value": 1}, {"name": "普通用户", "value": 0, "selected": True}]

		jsondata = {"id": s.user.id,"name":s.name,"username":s.user.username,"email":s.user.email,"phone":s.phone,
				    "weixin":s.weixin,"group_list":group_list,"role_list":role_list,"lnvalid_date":s.lnvalid_date,
					"is_superuser":s.user.is_superuser,"info":s.info}

		return JsonResponse(jsondata)

	def post(self, request):
		ret = {"status": 0, 'msg': '修改成功'}

		user_form = CreateUserForm(request.POST)
		profile_form = CreateProfileForm(request.POST)
		uid = request.POST.get('id', None)
		username = request.POST.get('username', None)
		is_superuser = request.POST.get('is_superuser', None)
		#is_superuser = request.POST.getlist('is_superuser', None)
		print (uid,username,is_superuser)
		info = request.POST.get('info', None)
		print(info)
		user = User.objects.get(id=uid)
		groups = Group.objects.all()

		if user_form.is_valid() and profile_form.is_valid():
			print(user_form.cleaned_data)
			user.username=user_form.cleaned_data['username']
			user.email=user_form.cleaned_data['email']
			user.is_superuser=user_form.cleaned_data['is_superuser']
			user.save()

			print('用户表更新完成OK...')

			profile = Profile.objects.filter(user=user)

			profile.update(
				name=profile_form.cleaned_data['name'],
				lnvalid_date=profile_form.cleaned_data['lnvalid_date'],
				phone=profile_form.cleaned_data['phone'],
				weixin=profile_form.cleaned_data['weixin'],
				info=info
			)
			print('Profile表更新完成OK...')

			if request.POST.getlist('select', None):
				user.groups.set(request.POST.getlist('select'))
			else:
				user.groups.clear()
			print('用户组更新完成OK....')
		else:
			ret = {"status": 1, 'msg': '修改失败'}

		return JsonResponse(ret)


# 查看用户
class UserDetailView(LoginRequiredMixin, DetailView):
	template_name = 'user/user_detail.html'
	model = User

	def get_context_data(self, **kwargs):
		context = super(UserDetailView, self).get_context_data(**kwargs)

		context['groups'] = Group.objects.all()
		return context


# 修改用户的用户组
class UserModifyGroupView(LoginRequiredMixin, View):

	def post(self, request):
		ret = {"status": 0, 'msg': '修改成功'}
		groups = request.POST.getlist('groups[]', None)
		uid = request.POST.get('uid', None)
		print(request.POST)
		print(uid)

		try:
			user = User.objects.get(pk=uid)
			if groups:
				try:
					user.groups.set(groups)
				except Exception:
					ret['status'] = 1
					ret['msg'] = '其他错误，修改失败请联系管理员'
			else:
				user.groups.clear()
		except User.DoesNotExist:
			ret['status'] = 1
			ret['msg'] = '没有此用户，修改失败'
		except Exception:
			ret['status'] = 1
			ret['msg'] = '其他错误，请联系系统管理员'

		return JsonResponse(ret)

# 管理员设置密码
class UserFixPasswordView(LoginRequiredMixin, View):

	def get(self,requst):
		return render_to_response('user-password.html', locals())

	def post(self, request):
		ret = {"status":0}
		username = request.user.username
		user = User.objects.get(username=username)
		old_password = request.POST.get('old_password', None)
		new_password = request.POST.get('new_password', None)
		again_password = request.POST.get('again_password', None)
		if check_password(old_password,user.password):

			if new_password and again_password:
				if new_password == again_password:
					try:
						user = User.objects.get(username=username)
						user.set_password(new_password)
						user.save()
						ret['msg'] = '设置密码成功'
					except User.DoesNotExist:
						ret['status'] = 1
						ret['msg'] = '此用户不存在，设置失败'
					except Exception:
						ret['status'] = 1
						ret['msg'] = '其他错误，设置失败，请联系系统管理员'
				else:
					ret['status'] = 1
					ret['msg'] = '密码不一致，设置失败'
			else:
				ret['status'] = 1
				ret['msg'] = '空密码设置失败'
		else:
			ret['status'] = 1
			ret['msg'] = '输入老密码校验失败,请确认后重新输入'

		return JsonResponse(ret)



# 管理员设置密码
class UserSetPasswordView(LoginRequiredMixin, View):

	def post(self, request):
		ret = {"status":0}

		password_1 = request.POST.get('password_1', None)
		password_2 = request.POST.get('password_2', None)
		uid = request.POST.get('id', None)
		pr = Profile.objects.get(id=uid)

		if password_1 and password_2:
			if password_1 == password_2:
				try:
					user = User.objects.get(pk=pr.user.id)
					user.set_password(password_1)
					user.save()
					ret['msg'] = '设置密码成功'
				except User.DoesNotExist:
					ret['status'] = 1
					ret['msg'] = '此用户不存在，设置失败'
				except Exception:
					ret['status'] = 1
					ret['msg'] = '其他错误，设置失败，请联系系统管理员'
			else:
				ret['status'] = 1
				ret['msg'] = '密码不一致，设置失败'
		else:
			ret['status'] = 1
			ret['msg'] = '空密码设置失败'

		return JsonResponse(ret)


# 设置用户权限
class UserSetPermView(LoginRequiredMixin, TemplateView):
	template_name = 'user/user_set_perm.html'

	def get_context_data(self, **kwargs):
		uid = self.request.GET.get('uid')
		context = super(UserSetPermView, self).get_context_data(**kwargs)
		context['perm_list'] = Permission.objects.all().exclude(name__regex='[a-zA-Z0-9]')
		context['user_obj'] = User.objects.get(pk=uid)

		return context

	def post(self, request):
		ret = {'status': 0}

		uid = request.POST.get('user_id')
		try:
			user = User.objects.get(pk=uid)
			user.user_permissions = request.POST.getlist('perm_list[]')
			user.save()
			ret['msg'] = '设置成功'
		except Exception:
			ret['status'] = 1
			ret['msg'] = '设置失败'

		return JsonResponse(ret)


# 获取用户信息
class UserGetListView(LoginRequiredMixin, View):

	def get(self, request):
		users = User.objects.values('id', 'email', 'username')
		print(request)
		return JsonResponse(list(users), safe=False)

class UserAPIView(LoginRequiredMixin, TemplateView):
	template_name = 'index.html'

	def get(self, request):
		jsdata = {
			  "code": 0,
			  "msg": "",
			  "count": 1000,
			  "data": [
				{
				  "id": 10000,
				  "username": "user-0",
				  "sex": "女",
				  "city": "城市-0",
				  "sign": "签名-0",
				  "experience": 255,
				  "logins": 24,
				  "wealth": 82830700,
				  "classify": "作家",
				  "score": 57
				},
				{
				  "id": 10001,
				  "username": "user-1",
				  "sex": "男",
				  "city": "城市-1",
				  "sign": "签名-1",
				  "experience": 884,
				  "logins": 58,
				  "wealth": 64928690,
				  "classify": "词人",
				  "score": 27
				},
				{
				  "id": 10002,
				  "username": "user-2",
				  "sex": "女",
				  "city": "城市-2",
				  "sign": "签名-2",
				  "experience": 650,
				  "logins": 77,
				  "wealth": 6298078,
				  "classify": "酱油",
				  "score": 31
				},
				{
				  "id": 10003,
				  "username": "user-3",
				  "sex": "女",
				  "city": "城市-3",
				  "sign": "签名-3",
				  "experience": 362,
				  "logins": 157,
				  "wealth": 37117017,
				  "classify": "诗人",
				  "score": 68
				},
				{
				  "id": 10004,
				  "username": "user-4",
				  "sex": "男",
				  "city": "城市-4",
				  "sign": "签名-4",
				  "experience": 807,
				  "logins": 51,
				  "wealth": 76263262,
				  "classify": "作家",
				  "score": 6
				},
				{
				  "id": 10005,
				  "username": "user-5",
				  "sex": "女",
				  "city": "城市-5",
				  "sign": "签名-5",
				  "experience": 173,
				  "logins": 68,
				  "wealth": 60344147,
				  "classify": "作家",
				  "score": 87
				},
				{
				  "id": 10006,
				  "username": "user-6",
				  "sex": "女",
				  "city": "城市-6",
				  "sign": "签名-6",
				  "experience": 982,
				  "logins": 37,
				  "wealth": 57768166,
				  "classify": "作家",
				  "score": 34
				},
				{
				  "id": 10007,
				  "username": "user-7",
				  "sex": "男",
				  "city": "城市-7",
				  "sign": "签名-7",
				  "experience": 727,
				  "logins": 150,
				  "wealth": 82030578,
				  "classify": "作家",
				  "score": 28
				},
				{
				  "id": 10008,
				  "username": "user-8",
				  "sex": "男",
				  "city": "城市-8",
				  "sign": "签名-8",
				  "experience": 951,
				  "logins": 133,
				  "wealth": 16503371,
				  "classify": "词人",
				  "score": 14
				},
				{
				  "id": 10009,
				  "username": "user-9",
				  "sex": "女",
				  "city": "城市-9",
				  "sign": "签名-9",
				  "experience": 484,
				  "logins": 25,
				  "wealth": 86801934,
				  "classify": "词人",
				  "score": 75
				}
			  ]
			}
		return JsonResponse(jsdata)