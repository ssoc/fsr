# ~*~ coding: utf-8 ~*~
import os
import configparser
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, 'conf/db.conf'))

#mysql config
mysql_db = config.get('mysql_config','db')
mysql_host = config.get('mysql_config','host')
mysql_user = config.get('mysql_config','user')
mysql_passwd = config.get('mysql_config','passwd')
mysql_charset = config.get('mysql_config','charset')
mysql_timeout = int(config.get('mysql_config','timeout'))

#mail config
#mail_host = config.get('mail_celery','host')
#mail_username = config.get('mail_celery','username')
#mail_password = config.get('mail_celery','password')
#mail_port = config.get('mail_celery','port')

#celery config
BROKER_URL = config.get('CeleryConfig','BROKER_URL')
CELERY_RESULT_BACKEND = config.get('CeleryConfig','CELERY_RESULT_BACKEND')
CELERY_ACCEPT_CONTENT = config.get('CeleryConfig','CELERY_ACCEPT_CONTENT')
CELERY_TASK_SERIALIZER = config.get('CeleryConfig','CELERY_TASK_SERIALIZER')
CELERY_RESULT_SERIALIZER = config.get('CeleryConfig','CELERY_RESULT_SERIALIZER')
CELERY_TIMEZONE = config.get('CeleryConfig','CELERY_TIMEZONE')